﻿namespace Игра_Разума
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox_Logo = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox_Rec = new System.Windows.Forms.PictureBox();
            this.pictureBox_MiniLogo = new System.Windows.Forms.PictureBox();
            this.pictureBox_Speed = new System.Windows.Forms.PictureBox();
            this.label3_Temp = new System.Windows.Forms.Label();
            this.label_NoCamera = new System.Windows.Forms.Label();
            this.FPS = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Rec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_MiniLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Speed)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_Logo
            // 
            this.pictureBox_Logo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Logo.Image")));
            this.pictureBox_Logo.Location = new System.Drawing.Point(223, 87);
            this.pictureBox_Logo.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox_Logo.Name = "pictureBox_Logo";
            this.pictureBox_Logo.Size = new System.Drawing.Size(825, 519);
            this.pictureBox_Logo.TabIndex = 0;
            this.pictureBox_Logo.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(555, 610);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            // 
            // pictureBox_Rec
            // 
            this.pictureBox_Rec.Location = new System.Drawing.Point(303, 299);
            this.pictureBox_Rec.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox_Rec.Name = "pictureBox_Rec";
            this.pictureBox_Rec.Size = new System.Drawing.Size(320, 112);
            this.pictureBox_Rec.TabIndex = 3;
            this.pictureBox_Rec.TabStop = false;
            // 
            // pictureBox_MiniLogo
            // 
            this.pictureBox_MiniLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_MiniLogo.Image")));
            this.pictureBox_MiniLogo.Location = new System.Drawing.Point(33, 13);
            this.pictureBox_MiniLogo.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox_MiniLogo.Name = "pictureBox_MiniLogo";
            this.pictureBox_MiniLogo.Size = new System.Drawing.Size(408, 155);
            this.pictureBox_MiniLogo.TabIndex = 4;
            this.pictureBox_MiniLogo.TabStop = false;
            // 
            // pictureBox_Speed
            // 
            this.pictureBox_Speed.Location = new System.Drawing.Point(1016, 50);
            this.pictureBox_Speed.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox_Speed.Name = "pictureBox_Speed";
            this.pictureBox_Speed.Size = new System.Drawing.Size(100, 92);
            this.pictureBox_Speed.TabIndex = 5;
            this.pictureBox_Speed.TabStop = false;
            // 
            // label3_Temp
            // 
            this.label3_Temp.AutoSize = true;
            this.label3_Temp.Location = new System.Drawing.Point(843, 31);
            this.label3_Temp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3_Temp.Name = "label3_Temp";
            this.label3_Temp.Size = new System.Drawing.Size(46, 17);
            this.label3_Temp.TabIndex = 6;
            this.label3_Temp.Text = "label3";
            // 
            // label_NoCamera
            // 
            this.label_NoCamera.AutoSize = true;
            this.label_NoCamera.Location = new System.Drawing.Point(52, 752);
            this.label_NoCamera.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_NoCamera.Name = "label_NoCamera";
            this.label_NoCamera.Size = new System.Drawing.Size(0, 17);
            this.label_NoCamera.TabIndex = 9;
            // 
            // FPS
            // 
            this.FPS.AutoSize = true;
            this.FPS.Location = new System.Drawing.Point(47, 352);
            this.FPS.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FPS.Name = "FPS";
            this.FPS.Size = new System.Drawing.Size(46, 17);
            this.FPS.TabIndex = 10;
            this.FPS.Text = "label3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(1274, 882);
            this.Controls.Add(this.FPS);
            this.Controls.Add(this.label_NoCamera);
            this.Controls.Add(this.label3_Temp);
            this.Controls.Add(this.pictureBox_Speed);
            this.Controls.Add(this.pictureBox_MiniLogo);
            this.Controls.Add(this.pictureBox_Rec);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox_Logo);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Rec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_MiniLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Speed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_Logo;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox_Rec;
        private System.Windows.Forms.PictureBox pictureBox_MiniLogo;
        private System.Windows.Forms.PictureBox pictureBox_Speed;
        private System.Windows.Forms.Label label3_Temp;
        private System.Windows.Forms.Label label_NoCamera;
        private System.Windows.Forms.Label FPS;
    }
}

