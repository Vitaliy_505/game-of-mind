﻿    using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Video.VFW;

namespace Игра_Разума
{
    public partial class Form1 : Form
    {
        class HookKeys
        {
            public HotKey logoKey,
            clockKey,
            recKey,
            playKey1,
            playKey2,
            changeClockKey1,
            changeClockKey2,
            speedKey1,
            speedKey2,
            speedKey3,
            exit;

            public HookKeys()
            {
                logoKey = new HotKey(Keys.D0, KeyModifiers.Alt);
                clockKey = new HotKey(Keys.D1, KeyModifiers.Alt);
                recKey = new HotKey(Keys.R, KeyModifiers.Alt);
                playKey1 = new HotKey(Keys.P, KeyModifiers.Alt);
                playKey2 = new HotKey(Keys.L, KeyModifiers.Alt);
                changeClockKey1 = new HotKey(Keys.D3, KeyModifiers.Alt);
                changeClockKey2 = new HotKey(Keys.D4, KeyModifiers.Alt);
                speedKey1 = new HotKey(Keys.Z, KeyModifiers.Alt);
                speedKey2 = new HotKey(Keys.X, KeyModifiers.Alt);
                speedKey3 = new HotKey(Keys.C, KeyModifiers.Alt);
                exit = new HotKey(Keys.Enter, KeyModifiers.Alt);
            }

            public void Register(Form form)
            {
                logoKey.Register(form);
                clockKey.Register(form);
                recKey.Register(form);
                playKey1.Register(form);
                playKey2.Register(form);
                changeClockKey1.Register(form);
                changeClockKey2.Register(form);
                speedKey1.Register(form);
                speedKey2.Register(form);
                speedKey3.Register(form);
                exit.Register(form);
            }

            public void Unregister()
            {
                logoKey.Unregister();
                clockKey.Unregister();
                recKey.Unregister();
                playKey1.Unregister();
                playKey2.Unregister();
                changeClockKey1.Unregister();
                changeClockKey2.Unregister();
                speedKey1.Unregister();
                speedKey2.Unregister();
                speedKey3.Unregister();
                exit.Unregister();
            }

            ~HookKeys()
            {
                logoKey.Unregister();
                clockKey.Unregister();
                recKey.Unregister();
                playKey1.Unregister();
                playKey2.Unregister();
                changeClockKey1.Unregister();
                changeClockKey2.Unregister();
                speedKey1.Unregister();
                speedKey2.Unregister();
                speedKey3.Unregister();
                exit.Unregister();
            }
        }
        private bool start = false;
        private bool startRecord;
        private const int TIME_OF_RECORD = 11;
        private const int TIME = 60 * 60 - 1;
        private const int TIME_TO_CHANGE = 1 * 60;
        private const int DESIGN_BORDER = 40;
        private int RESOLUTION_HEIGHT = 480;
        private int RESOLUTION_WIDTH = 640;
        private const int PLAYBOX_WIDTH = 800;
        private const int PLAYBOX_HEIGHT = 600;
        double Time1 = 0;
        int recTime = 0;

        System.Drawing.Text.PrivateFontCollection RussoFont;

        HookKeys hookKeys = null;

        //Для камеры
        FilterInfoCollection videoDevices;
        VideoCaptureDevice videoSource;

        AVIWriter writer;
        Bitmap overlayPic;

        bool writerIsRunning = false;
        bool bgwIsBusy = false;
        int cameraFrameRate = -1;

        BackgroundWorker bgWorker;

        bool CamIsRec = false;

        PictureBox pictureBox_PlayVideo;
        //end Для камеры

        // Для воспроизведения видео
        FileVideoSource videoToPlay;
        bool videoIsPlaying = false;
        string namePicToAdd = "03_cam1.png";
        string filename = "test.avi";
        bool timeToAddPic = false;
        Bitmap PicToAdd;
        // end Для воспроизведения видео

        // для FPS
        System.Windows.Forms.Timer timerToFPS = null;
        Stopwatch stopWatch = null;
        float tempFPS = 0;
        int ticks = 0;
        //

        private delegate void DelegateHookKeyPress();
        DelegateHookKeyPress delegateHookKeyPress;

        public Form1()
        {
            InitializeComponent();
            // show device list
            try
            {
                // enumerate video devices
                videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                if (videoDevices.Count == 0)
                {
                    throw new Exception();
                }
            }
            catch
            {

            }
        }

        private void Restart()
        {
            LogoStart();
            StopRec();
            StopClock();
            StopSpeed();
            start = false;
            StopPlayVideo();
            timeToAddPic = false;
            if (File.Exists("test.avi"))
                File.Delete("test.avi");
        }

        private void SetLogo()
        {
            //настройка pictureBox1_Logo 
            pictureBox_Logo.Location = new Point((this.Width - pictureBox_Logo.Image.Width) / 2, (this.Height - pictureBox_Logo.Image.Height) / 2);
            pictureBox_Logo.Size = this.Size;
            pictureBox_MiniLogo.Visible = false;
            pictureBox_MiniLogo.Location = new Point(DESIGN_BORDER, DESIGN_BORDER);
        }

        private void LogoStart()
        {
            startRecord = false;
            pictureBox_Logo.Visible = true;
            pictureBox_Rec.Visible = false;
            pictureBox_MiniLogo.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
            ChangeLabel2("нормальный", Color.Green);
        }

        private void LogoStop()
        {
            pictureBox_MiniLogo.Visible = true;
            pictureBox_Logo.Visible = false;
        }

        private void SetClock()
        {
            label1.Font = new Font(RussoFont.Families[0], 200);
            label1.Location = new Point((this.Size.Width - label1.Size.Width) / 2, (this.Size.Height - label1.Size.Height) / 2);
            label1.ForeColor = Color.Gray;
            Time1 = TIME;

            PrintTime();

            timer1.Interval = 1000;
            label1.Visible = false;
        }

        private void SetClockAgain()
        {
            label1.Font = new Font(RussoFont.Families[0], 200);
            label1.Location = new Point((this.Size.Width - label1.Size.Width) / 2, (this.Size.Height - label1.Size.Height) / 2);

            pictureBox_MiniLogo.Visible = true;
        }

        private void StartClock()
        {
            label1.Visible = true;
            timer1.Start();
        }

        private void StopClock()
        {
            timer1.Stop();
        }

        private void ChangeClock(int time)
        {
            Time1 += time;
            if (Time1 > TIME) Time1 = TIME;
            if (Time1 < 0) Time1 = 0;
            PrintTime();
        }

        private void IncClock(int time)
        {
            Time1 += time;
            if (Time1 > TIME) Time1 = TIME;
        }

        private void DecClock(int time)
        {
            Time1 -= time;
            if (Time1 < TIME_TO_CHANGE) Time1 = 0;
        }

        private void SetRec()
        {
            pictureBox_Rec.Size = new Size(50, 50);
            pictureBox_Rec.BackColor = Color.Red;
            pictureBox_Rec.Location = new Point((this.Size.Width - pictureBox_Rec.Size.Width) / 2, this.Size.Height - pictureBox_Rec.Size.Height - DESIGN_BORDER);
            pictureBox_Rec.Visible = false;
        }

        private void RecToFPS()
        {
            if (videoDevices.Count != 0)
            {
                videoSource = new VideoCaptureDevice(videoDevices[0].MonikerString);
                videoSource.NewFrame += new NewFrameEventHandler(videoSourceToFPS_NewFrame);
                videoSource.Start();
                System.Threading.Thread.Sleep(20);
                timerToFPS = new System.Windows.Forms.Timer();
                timerToFPS.Tick += new EventHandler(timerToFPS_Tick);
                timerToFPS.Start();
                recTime = (int)Time1;
            }
        }

        private void StopRecToFPS()
        {
            if (videoDevices.Count != 0)
            {
                timerToFPS.Dispose();
                cameraFrameRate = (int)(tempFPS / ticks) - 1;

                /*FPS.ForeColor = Color.White;
                FPS.Font = new Font("Times New Roman", 50);
                FPS.Text = cameraFrameRate.ToString();*/

                tempFPS = 0;
                ticks = 0;
                if (videoSource != null)
                {
                    videoSource.SignalToStop();
                    System.Threading.Thread.Sleep(50);
                    videoSource = null;
                }
                GC.Collect();
            }
        }

        private void StartRec()
        {
            if (videoDevices.Count != 0 && cameraFrameRate > 0)
            {
                startRecord = true;
                CamIsRec = true;
                pictureBox_Rec.Visible = true;
                
                if (writerIsRunning)
                {
                    StopCamera();
                    System.Threading.Thread.Sleep(10);
                }
                try
                {
                    StartCamera();
                }
                catch (Exception e)
                {
                    MessageBox.Show("StartCamera Exception: " + e);
                }
                //recTime = (int)Time1;
            }
        }

        private void StopRec()
        {
            if (videoDevices.Count != 0) StopCamera();
            CamIsRec = false;
            pictureBox_Rec.Visible = false;
        }

        private void PlayVideoRec()
        {
            pictureBox_MiniLogo.Visible = false;
            label1.Font = new Font(RussoFont.Families[0], 24);
            label1.Location = new Point(DESIGN_BORDER, this.Size.Height - label1.Size.Height - DESIGN_BORDER + 20);
            StartSpeedLocationToPlayVideo();

            pictureBox_PlayVideo = new PictureBox();

            pictureBox_PlayVideo.Size = new Size(PLAYBOX_WIDTH, PLAYBOX_HEIGHT);

            pictureBox_PlayVideo.Location = new Point((this.Size.Width - pictureBox_PlayVideo.Width) / 2, (this.Size.Height - pictureBox_PlayVideo.Height - DESIGN_BORDER) / 2);// - DESIGN_BORDER - label1.Size.Height);

            this.Controls.Add(pictureBox_PlayVideo);

            bgWorker = new BackgroundWorker();
            bgWorker.DoWork += bgWorker_DoWork;
            if (!bgwIsBusy)
                bgWorker.RunWorkerAsync();
            bgwIsBusy = true;
        }

        private void SetSpeed()
        {
            pictureBox_Speed.Size = new Size(75, 75);
            pictureBox_Speed.Location = new Point(this.Size.Width - pictureBox_Speed.Width - DESIGN_BORDER, DESIGN_BORDER);
            pictureBox_Speed.BackColor = Color.Green;
            pictureBox_Speed.Visible = false;

            label2.Font = new Font(RussoFont.Families[0], 30);
            ChangeLabel2("нормальный", Color.Green);

            label3_Temp.Text = "темп игры";
            label3_Temp.Font = new Font(RussoFont.Families[0], 30);
            label3_Temp.ForeColor = Color.Gray;
            
            label2.Visible = false;
            label3_Temp.Visible = false;
        }

        private void StartSpeed()
        {
            pictureBox_Speed.Visible = true;
            label2.Visible = true;
            label3_Temp.Visible = true;
            //label3_Temp.Text = "темп игры";

            pictureBox_Speed.Size = new Size(50, 50);
            label2.Font = new Font(RussoFont.Families[0], 24);
            label3_Temp.Font = new Font(RussoFont.Families[0], 24);

            pictureBox_Speed.Location = new Point(this.Size.Width - pictureBox_Speed.Width - DESIGN_BORDER, DESIGN_BORDER);
            label3_Temp.Location = new Point(this.Size.Width - label3_Temp.Size.Width - pictureBox_Speed.Size.Width - DESIGN_BORDER - 10, pictureBox_Speed.Location.Y - 11);
            label2.Location = new Point(this.Size.Width - label2.Size.Width - pictureBox_Speed.Size.Width - DESIGN_BORDER - 10, pictureBox_Speed.Location.Y + pictureBox_Speed.Size.Height - label2.Size.Height + 7);
        }

        private void StartSpeedLocationToPlayVideo()
        {
            pictureBox_Speed.Size = new Size(30, 30);
            label2.Font = new Font(RussoFont.Families[0], 12);
            label3_Temp.Font = new Font(RussoFont.Families[0], 12);
            pictureBox_Speed.Location = new Point(this.Size.Width - pictureBox_Speed.Width - DESIGN_BORDER, this.Size.Height - pictureBox_Speed.Height - DESIGN_BORDER);
            /*label2.Location = new Point(this.Size.Width - label2.Size.Width - pictureBox_Speed.Size.Width - DESIGN_BORDER - 10, pictureBox_Speed.Location.Y + pictureBox_Speed.Size.Height / 2);
            label3_Temp.Text = "темп игры:";
            label3_Temp.Location = new Point(this.Size.Width - label3_Temp.Size.Width - label2.Location.X - DESIGN_BORDER - 10, pictureBox_Speed.Location.Y + pictureBox_Speed.Size.Height / 2);*/
            label3_Temp.Location = new Point(this.Size.Width - label3_Temp.Size.Width - pictureBox_Speed.Size.Width - DESIGN_BORDER - 5, pictureBox_Speed.Location.Y - 5);
            label2.Location = new Point(this.Size.Width - label2.Size.Width - pictureBox_Speed.Size.Width - DESIGN_BORDER - 5, pictureBox_Speed.Location.Y + pictureBox_Speed.Size.Height - label2.Size.Height + 4);
        }

        private void ChangeLabel2(string s, Color c)
        {
            pictureBox_Speed.BackColor = c;
            label2.Text = s;
            label2.ForeColor = c;
            if (!videoIsPlaying) label2.Location = new Point(this.Size.Width - label2.Size.Width - pictureBox_Speed.Size.Width - DESIGN_BORDER - 5, pictureBox_Speed.Location.Y + pictureBox_Speed.Size.Height - label2.Size.Height + 4);
            else label2.Location = new Point(this.Size.Width - label2.Size.Width - pictureBox_Speed.Size.Width - DESIGN_BORDER - 5, pictureBox_Speed.Location.Y + pictureBox_Speed.Size.Height / 2);
        }

        private void StopSpeed()
        {
            pictureBox_Speed.Visible = false;
            label2.Visible = false;
            label3_Temp.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Screen.AllScreens.Length > 1)
                this.Location = Screen.AllScreens[1].WorkingArea.Location;
            //изменение окна
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;

            RussoFont = new System.Drawing.Text.PrivateFontCollection();
            RussoFont.AddFontFile("Russo_One.ttf");

            PicToAdd = (Bitmap)Image.FromFile("04_png.png");
            PicToAdd = ResizePicture(PicToAdd, PLAYBOX_WIDTH, PLAYBOX_HEIGHT);

            timer1.Tick += new EventHandler(timer1_Tick);

            SetLogo();
            SetClock();
            SetRec();
            SetSpeed();
            
            hookKeys = new HookKeys();
            SetHookKey();
            delegateHookKeyPress = new DelegateHookKeyPress(HookKeyPress);
            delegateHookKeyPress.Invoke();

            //RecToFPS();
        }

        public void SetHookKey()
        {
            //LOGO
            hookKeys.logoKey.Pressed += (o, e) =>
            {
                Restart();
                e.Handled = true;
            };
            //CLOCK
            hookKeys.clockKey.Pressed += (o, e) =>
            {
                LogoStop();
                if (!start)
                {
                    start = true;
                    SetClock();
                }
                SetClockAgain();
                StartClock();
                StartSpeed();

                StopPlayVideo();
                timeToAddPic = false;

                if (videoDevices.Count == 0)
                    NoCamera();
                if (cameraFrameRate < 0)
                   RecToFPS();
                e.Handled = true;
            };
            //RECORD
            hookKeys.recKey.Pressed += (o, e) =>
            {
                if (start)
                {
                    SetClockAgain();
                    StartClock();
                    StartSpeed();

                    StopPlayVideo();
                    timeToAddPic = false;

                    ChangeOverlayPic("03_cam1.png");

                    StartRec();
                }
                e.Handled = true;
            };
            //PLAY VIDEO 
            hookKeys.playKey1.Pressed += (o, e) =>
            {
                if (start)
                {
                    if (videoDevices.Count != 0)
                        if (!CamIsRec)
                            if (File.Exists("test.avi"))
                            {
                                StopPlayVideo();
                                PlayVideoRec();
                            }
                }
                timeToAddPic = false;
                e.Handled = true;
            };
            //PLAY VIDEO WITH ADDED PIC
            hookKeys.playKey2.Pressed += (o, e) =>
            {
                timeToAddPic = !timeToAddPic;
            };
            //CHANGE TIME (TIME_TO_CHANGE)
            hookKeys.changeClockKey1.Pressed += (o, e) =>
            {
                if (start)
                {
                    ChangeClock(TIME_TO_CHANGE);
                }
                e.Handled = true;
            };
            //CHANGE TIME (- TIME_TO_CHANGE)
            hookKeys.changeClockKey2.Pressed += (o, e) =>
            {
                if (start)
                {
                    ChangeClock(-TIME_TO_CHANGE);
                }
                e.Handled = true;
            };

            //CHANGE SPEED (Z)
            hookKeys.speedKey1.Pressed += (o, e) =>
            {
                if (start)
                {
                    ChangeLabel2("нормальный", Color.Green);
                }
                e.Handled = true;
            };

            //CHANGE SPEED (X)
            hookKeys.speedKey2.Pressed += (o, e) =>
            {
                if (start)
                {
                    ChangeLabel2("стоит поторопиться", Color.Yellow);
                }
                e.Handled = true;
            };

            //CHANGE SPEED (C)
            hookKeys.speedKey3.Pressed += (o, e) =>
            {
                if (start)
                {
                    ChangeLabel2("берите подсказку", Color.Red);
                }
                e.Handled = true;
            };
            //EXIT
            hookKeys.exit.Pressed += (o, e) =>
            {
                Application.Exit();
                e.Handled = true;
            };

        }

        private void HookKeyPress()
        {
            hookKeys.Register(this);
        }

        private void PrintTime()
        {
            int min = (int)Time1 / 60;
            int sec = (int)Time1 % 60;
            string s = (min > 9 ? min.ToString() : "0" + (string)min.ToString()) + " : " + (sec > 9 ? sec.ToString() : "0" + (string)sec.ToString());
            label1.Text = s;
        }

        private void timerToFPS_Tick(object sender, EventArgs e)
        {
            try
            {
                if (videoSource != null)
                {
                    int framesReceived = videoSource.FramesReceived;
                    if (stopWatch == null)
                    {
                        stopWatch = new Stopwatch();
                        stopWatch.Start();
                    }
                    else
                    {
                        stopWatch.Stop();
                        System.Threading.Thread.Sleep(20);
                        //MessageBox.Show("YEA");
                        float fps = 1000.0f * framesReceived / stopWatch.ElapsedMilliseconds;
                        tempFPS += fps;
                        ticks++;

                        /*FPS.ForeColor = Color.White;
                        FPS.Font = new Font("Times New Roman", 50);
                        FPS.Text = fps.ToString();
                        cameraFrameRate = (int)fps;*/

                        stopWatch.Reset();
                        stopWatch.Start();
                    }
                }
            }
            catch (Exception Exc)
            { MessageBox.Show(Exc.ToString()); }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Time1 > 0)
                Time1 -= 0.5;
            else Time1 = 0;

            PrintTime();
            
            if (startRecord == false && ((int)Time1 / 60) < 55)
                StartRec();
            
            if ((recTime - Time1) % 60 >= TIME_OF_RECORD)
            {
                if (cameraFrameRate < 0)
                    StopRecToFPS();
                else
                    StopRec();
            }
            
            if (videoToPlay != null)
            {
                if (!videoToPlay.IsRunning)
                {
                    StopPlayVideo();
                    System.Threading.Thread.Sleep(10);
                    if (namePicToAdd == "03_cam1.png")
                    {
                        namePicToAdd = "03_cam2.png";
                        filename = "videoToAdd.avi";
                    }
                    else
                    {
                        namePicToAdd = "03_cam1.png";
                        filename = "test.avi";
                    }
                    ChangeOverlayPic(namePicToAdd);

                    PlayVideoRec();
                    System.Threading.Thread.Sleep(10);
                }
                //if (videoSource != null)
                

            }
            if (Time1 == 0)
            {
                Restart();
            }
        }

        private void StartCamera()
        {
            // create new AVI file and open it
            if (writer == null)
                writer = new AVIWriter("DIB ");

            writer.Codec = "XviD";

            if (File.Exists("test.avi"))
            {
                File.Delete("test.avi");
                Thread.Sleep(50);
            }

            // create video sources
            videoSource = new VideoCaptureDevice(videoDevices[0].MonikerString);

            //videoSource пишет в режиме VideoCapabilities[0]

            /*int max_height = 0, camInd = 0;
            for (int i = 0; i < videoSource.VideoCapabilities.Length; i++)
            {
                if (videoSource.VideoCapabilities[i].AverageFrameRate == 30)
                    if ((double)(videoSource.VideoCapabilities[i].FrameSize.Height * 1.0 / videoSource.VideoCapabilities[i].FrameSize.Width) == (double)(9 * 1.0 / 16))
                        if (videoSource.VideoCapabilities[i].FrameSize.Height > max_height)
                        {
                            max_height = videoSource.VideoCapabilities[i].FrameSize.Height;
                            camInd = i;
                        }
            }*/

            // set the Resolution
            if (videoSource.VideoCapabilities != null)
            {
                //FPS.Text = "VideoCapabilities.Length = " + videoSource.VideoCapabilities.Length.ToString();
                RESOLUTION_HEIGHT = videoSource.VideoCapabilities[0].FrameSize.Height;
                RESOLUTION_WIDTH = videoSource.VideoCapabilities[0].FrameSize.Width;
            }
            /*else
            {
                RESOLUTION_HEIGHT = 640;
                RESOLUTION_WIDTH = 480;
            }*/

            /*RESOLUTION_HEIGHT = videoSource.VideoCapabilities[camInd].FrameSize.Height;
            RESOLUTION_WIDTH = videoSource.VideoCapabilities[camInd].FrameSize.Width;

            writer.FrameRate = videoSource.VideoCapabilities[camInd].AverageFrameRate;
            cameraFrameRate = writer.FrameRate;

            videoSource.DesiredFrameRate = cameraFrameRate;
            videoSource.DesiredFrameSize = videoSource.VideoCapabilities[camInd].FrameSize;
            videoSource.VideoResolution = videoSource.VideoCapabilities[camInd];*/

            videoSource.NewFrame += new NewFrameEventHandler(videoSource_NewFrame);
            videoSource.Start();
            System.Threading.Thread.Sleep(20);

            //writer.FrameRate = videoSource.VideoCapabilities[4].FrameRate;
            //writer.FrameRate = videoSource.VideoCapabilities[9].FrameRate;
            //writer.FrameRate = videoSource.VideoCapabilities[0].FrameRate;
            //writer.FrameRate = videoSource.VideoCapabilities[0].AverageFrameRate;

            //writer.FrameRate = videoSource.VideoCapabilities[0].AverageFrameRate;

            writer.FrameRate = cameraFrameRate;


            //cameraFrameRate = writer.FrameRate;
            //writer.FrameRate = cameraFrameRate;
            /*
            writer.FrameRate = videoSource.VideoCapabilities[camInd].AverageFrameRate;
            cameraFrameRate = writer.FrameRate;*/

            writer.Open("test.avi", RESOLUTION_WIDTH, RESOLUTION_HEIGHT);
            writerIsRunning = true;
            recTime = (int)Time1;
        }

        private void StopCamera()
        {
            if (videoSource != null)
            {
                videoSource.SignalToStop();
                System.Threading.Thread.Sleep(50);
                videoSource = null;
            }
            if (writer != null)
            {
                writer.Close();
                System.Threading.Thread.Sleep(5);
                writer = null;
            }
            GC.Collect();
        }

        private Bitmap ResizePicture(Image pic, int width, int height)
        {
            Bitmap original = new Bitmap(pic);
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(result))
                g.DrawImage(original, 0, 0, width, height);
            return result;
        }

        private Bitmap ResizePicture(Bitmap pic, int width, int height)
        {
            Bitmap original = new Bitmap(pic);
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(result))
                g.DrawImage(original, 0, 0, width, height);
            return result;
        }

        void videoSourceToFPS_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
        }

        void videoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            // create frame image
            Bitmap image = (Bitmap)eventArgs.Frame.Clone();
            //MessageBox.Show(image.Size.ToString());
            image.RotateFlip(RotateFlipType.Rotate180FlipY);

            //image = ResizePicture(image, RESOLUTION_WIDTH, RESOLUTION_HEIGHT);

            try
            {
                writer.AddFrame(image);
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception: " + e);
            }
        }

        private void videoToPlay_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            // get new frame
            Bitmap image = eventArgs.Frame;
            image = ResizePicture(image, PLAYBOX_WIDTH, PLAYBOX_HEIGHT);
            
            // process the frame
            var finalImage = new Bitmap(overlayPic.Width, overlayPic.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            var graphics = Graphics.FromImage(finalImage);
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;

            graphics.DrawImage(image, 0, 0);
            graphics.DrawImage(overlayPic, 0, 0);

            if (timeToAddPic)
            {
                if (filename == "test.avi")
                {
                    var newFinalImage = new Bitmap(PicToAdd.Width, PicToAdd.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    graphics = Graphics.FromImage(newFinalImage);
                    graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;

                    graphics.DrawImage(finalImage, 0, 0);
                    graphics.DrawImage(PicToAdd, 0, 0);

                    finalImage = newFinalImage;
                }
            }

            if (pictureBox_PlayVideo != null) pictureBox_PlayVideo.Image = finalImage;

            //System.Threading.Thread.Sleep((int)(1000 / (cameraFrameRate)));
        }

        private void PlayVideo(string filename)
        {
            // create video source
            videoToPlay = new FileVideoSource(filename);
            // set NewFrame event handler
            videoToPlay.NewFrame += new NewFrameEventHandler(videoToPlay_NewFrame);
            // start the video source
            videoToPlay.Start();
            stopWatch = null;
        }

        private void StopPlayVideo()
        {
            if (bgwIsBusy)
            {
                bgwIsBusy = false;
                bgWorker.Dispose();
                System.Threading.Thread.Sleep(5);
                bgWorker = null;

                if (videoToPlay != null)
                {
                    videoToPlay.SignalToStop();
                    System.Threading.Thread.Sleep(5);
                    videoToPlay = null;
                }
                pictureBox_PlayVideo.Dispose();
                pictureBox_PlayVideo = null;
                System.Threading.Thread.Sleep(5);
                GC.Collect();
            }
            if (pictureBox_PlayVideo != null) pictureBox_PlayVideo.Visible = false;
        }

        private void NoCamera()
        {
            label_NoCamera.Font = new Font(RussoFont.Families[0], 30);
            label_NoCamera.ForeColor = Color.Gray;
            label_NoCamera.Location = new Point(DESIGN_BORDER, this.Size.Height - label_NoCamera.Height - DESIGN_BORDER);
            label_NoCamera.Text = "нет камеры";
        }

        private void ChangeOverlayPic(string filename)
        {
            overlayPic = (Bitmap)Image.FromFile(filename);
            overlayPic = ResizePicture(overlayPic, PLAYBOX_WIDTH, PLAYBOX_HEIGHT);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            hookKeys.Unregister();
            if (videoSource != null) StopCamera();
            if (File.Exists("test.avi"))
            {
                File.Delete("test.avi");
            }
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            PlayVideo(filename);
        }

    }
}
